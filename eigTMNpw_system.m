function system = eigTMNpw_system(dimension, delays, Omega, s, varargin)
% eigTMNpw_system: build struct of system parameters
% system = eigTMNpw_system(dimension, delays, Omega, initialTime, varargin)
%
% Required positional parameters
%   dimensions: vector [dX, dY] of the dimensions of the renewal equation and of
%     the differential equation
%   delays: vector of the delays (discrete delays or endpoints of distributed
%     delays) in ascending order
%   Omega: length of time interval of the evolution operator; in case of the
%     monodromy operator it is the period
%   s: initial time of the evolution operator
%
% Named parameters
%   AXX, AXY, AYX, AYY: function handles with arguments (t, par) defining the
%     current time terms of the equation (default: [])
%   BXX, BXY, BYX, BYY: cell arrays of function handles with arguments (t, par)
%     defining the discrete delay terms of the equation; the indices must
%     correspond to the vector of delays; define a coefficient as [] if not
%     used (default: {[]})
%   CXX, CXY, CYX, CYY: cell arrays of function handles with arguments
%     (t, theta, par) defining the distributed delay terms of the equation; the
%     indices must correspond to the vector of delays, C**{k} being the kernel
%     of the integral on [delays(k), delays(k-1)], assuming delays(0) = 0;
%     define a coefficient as [] if not used (default: {[]})
%   par: vector, cell array or struct with the model parameters, in the form
%     used by the coefficients
%   t, L: endpoints and number of the pieces in [0, Omega] (or in [0, 1])
%     for the piecewise approach (usually coming from the numerical periodic
%     solution); if both are given, they must be compatible; if only t is
%     given, L is computed; if only L is given, t is uniform; if none is given,
%     one piece is used
%
% Output
%   system: a struct with the following fields
%     - dX, dY: dimensions
%     - tau: sorted vector of delays, including 0 (tau(1) = 0,
%         tau(k+1) = tau_k, k in 1:p)
%     - Omega, s, AXX, AXY, AYX, AYY, par: as input
%     - BXX, BXY, BYX, BYY, CXX, CXY, CYX, CYY: as input, ensuring that they all
%         have the same length
%     - t, L: as input or computed
%     - flags: struct with fields corresponding to the coefficients, indicating
%         whether the coefficient is defined
%
% eigTMNpw, version 0.4, 2022-09-29
% Author: Davide LIESSI <name.surname@uniud.it>
%         Department of Mathematics, Computer Science and Physics
%         University of Udine, Italy
% Copyright (c) 2020-2022 Davide Liessi
% This file is distributed under the terms of the MIT license (see LICENSE.txt).

% TODO: allow delays to not be sorted as long as the indices correspond to the
%   coefficients; but this does not play well with distributed delays; maybe we
%   could specify coefficients and delays separately between discrete and
%   distributed delays and list distributed delays with both right and left
%   endpoints; maybe even delays alongside the corresponding coefficients;
%   automatically sort delays and coefficients

parser = inputParser;

checkDimension = @(x) isnumeric(x) && isreal(x) && (length(x) == 2) ...
                      && all(floor(x) == ceil(x)) && all(x >= 0) && any(x >= 1);
checkDelays = @(x) isnumeric(x) && isreal(x) && isvector(x) && all(x > 0) ...
                   && all(x == unique(x));
checkOmega = @(x) isnumeric(x) && isreal(x) && isscalar(x) && (x > 0);
checks = @(x) isnumeric(x) && isreal(x) && isscalar(x);
checkA = @(x) isempty(x) || (isa(x, 'function_handle') && (nargin(x) == 2));
checkB = @(x) iscell(x) && all(cellfun(checkA, x));
checkCk = @(x) isempty(x) || (isa(x, 'function_handle') && (nargin(x) == 3));
checkC = @(x) iscell(x) && all(cellfun(checkCk, x));
checkt = @(x) isnumeric(x) && isreal(x) && isvector(x) && all(x >= 0) ...
              && all(x == unique(x));
checkL = @(x) isnumeric(x) && isreal(x) && isscalar(x) ...
              && (floor(x) == ceil(x)) && (x >= 1);

addRequired(parser, 'dimension', checkDimension)
addRequired(parser, 'delays', checkDelays)
addRequired(parser, 'Omega', checkOmega)
addRequired(parser, 's', checks)
addParameter(parser, 'AXX', [], checkA)
addParameter(parser, 'AXY', [], checkA)
addParameter(parser, 'AYX', [], checkA)
addParameter(parser, 'AYY', [], checkA)
addParameter(parser, 'BXX', cell(1), checkB)
addParameter(parser, 'BXY', cell(1), checkB)
addParameter(parser, 'BYX', cell(1), checkB)
addParameter(parser, 'BYY', cell(1), checkB)
addParameter(parser, 'CXX', cell(1), checkC)
addParameter(parser, 'CXY', cell(1), checkC)
addParameter(parser, 'CYX', cell(1), checkC)
addParameter(parser, 'CYY', cell(1), checkC)
addParameter(parser, 'par', [])
addParameter(parser, 't', [], checkt)
addParameter(parser, 'L', [], checkL)

parse(parser, dimension, delays, Omega, s, varargin{:})



system.dX = parser.Results.dimension(1);
system.dY = parser.Results.dimension(2);
system.tau = [0, parser.Results.delays];
system.Omega = parser.Results.Omega;
system.s = parser.Results.s;

system.AXX = parser.Results.AXX;
system.AXY = parser.Results.AXY;
system.AYX = parser.Results.AYX;
system.AYY = parser.Results.AYY;

p = length(system.tau) - 1;
assert(length(parser.Results.BXX) <= p)
assert(length(parser.Results.BXY) <= p)
assert(length(parser.Results.BYX) <= p)
assert(length(parser.Results.BYY) <= p)
assert(length(parser.Results.CXX) <= p)
assert(length(parser.Results.CXY) <= p)
assert(length(parser.Results.CYX) <= p)
assert(length(parser.Results.CYY) <= p)

system.BXX = parser.Results.BXX;
system.BXY = parser.Results.BXY;
system.BYX = parser.Results.BYX;
system.BYY = parser.Results.BYY;
system.CXX = parser.Results.CXX;
system.CXY = parser.Results.CXY;
system.CYX = parser.Results.CYX;
system.CYY = parser.Results.CYY;

if length(system.BXX) < p
  system.BXX{p} = [];
end
if length(system.BXY) < p
  system.BXY{p} = [];
end
if length(system.BYX) < p
  system.BYX{p} = [];
end
if length(system.BYY) < p
  system.BYY{p} = [];
end
if length(system.CXX) < p
  system.CXX{p} = [];
end
if length(system.CXY) < p
  system.CXY{p} = [];
end
if length(system.CYX) < p
  system.CYX{p} = [];
end
if length(system.CYY) < p
  system.CYY{p} = [];
end

system.flags.AXX = ~isempty(system.AXX);
system.flags.AXY = ~isempty(system.AXY);
system.flags.AYX = ~isempty(system.AYX);
system.flags.AYY = ~isempty(system.AYY);
system.flags.BXX = cell(p, 1);
system.flags.BXY = cell(p, 1);
system.flags.BYX = cell(p, 1);
system.flags.BYY = cell(p, 1);
system.flags.CXX = cell(p, 1);
system.flags.CXY = cell(p, 1);
system.flags.CYX = cell(p, 1);
system.flags.CYY = cell(p, 1);
for k = 1 : p
  system.flags.BXX{k} = ~isempty(system.BXX{k});
  system.flags.BXY{k} = ~isempty(system.BXY{k});
  system.flags.BYX{k} = ~isempty(system.BYX{k});
  system.flags.BYY{k} = ~isempty(system.BYY{k});
  system.flags.CXX{k} = ~isempty(system.CXX{k});
  system.flags.CXY{k} = ~isempty(system.CXY{k});
  system.flags.CYX{k} = ~isempty(system.CYX{k});
  system.flags.CYY{k} = ~isempty(system.CYY{k});
end

system.par = parser.Results.par;

given_L = false;
if ~isempty(parser.Results.L)
  given_L = true;
  system.L = parser.Results.L;
end
given_t = false;
if ~isempty(parser.Results.t)
  given_t = true;
  system.t = parser.Results.t;
end
if given_t
  if given_L
    assert(length(system.t) == system.L+1)
  else
    system.L = length(system.t) - 1;
  end
else
  if given_L
    system.t = linspace(0, system.Omega, system.L+1);
  else
    system.L = 1;
    system.t = [0 system.Omega];
  end
end

assert(system.t(1) == 0);
if (system.t(end) == 1) && (system.Omega ~= 1)
  system.t = system.t * system.Omega;
end
assert(system.t(end) == system.Omega)
if ~iscolumn(system.t)
  system.t = system.t';
end

end
