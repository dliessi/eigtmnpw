function f = make_num_per_sol_mc(dX, dY, xlc, slc, flc)
% make_num_per_sol_mc: given a numerical periodic solution of a coupled
%   RE/DDE as computed by computed by MatCont with [1], build the
%   corresponding piecewise polynomial and return its function handle
%   f(i, t), with i the index of the component of the solution (components
%   1:dX for the RE, dX+1:dX+dY for the DDE) and t a vector of points to
%   evaluate
% f = make_num_per_sol_mc(dX, dY, xlc, slc, flc)
%
% Required positional parameters
%   dX, dY: dimensions of the RE and of the DDE
%   xlc, flc: columns corresponding to the periodic solution in the first
%             and fifth outputs of cont, respectively
%   slc: element corresponding to the periodic solution in the third
%        output of cont

ntst = slc.data.ntst;
ncol = slc.data.ncol;
M = slc.data.parametervalues(end);
MM = dX*M + dY*(M+1);
period = xlc(end-1);
mesh= flc(1:ntst+1);

coefficients = zeros(ntst, ncol+1, dX+dY);
for i = 1:dX
  profile = xlc(dX*(M-1)+i:MM:end-2);
  for j = 1:ntst
    coefficients(j, :, i) = ...
      polyfit(linspace(mesh(j), mesh(j+1), ncol+1)'-mesh(j), ...
              profile(((j-1)*ncol+1):(j*ncol+1)), ...
              ncol);
  end
end
for i = 1:dY
  profile = xlc((dX+dY)*M+i:MM:end-2);
  for j = 1:ntst
    coefficients(j, :, dX+i) = ...
      polyfit(linspace(mesh(j), mesh(j+1), ncol+1)'-mesh(j), ...
              profile(((j-1)*ncol+1):(j*ncol+1)), ...
              ncol);
  end
end

polynomials = cell(dX+dY, 1);
for i = 1:(dX+dY)
  polynomials{i} = mkpp(mesh, coefficients(:, :, i));
end

f = @(i, t) ppval(polynomials{i}, t/period-floor(t/period));

end
