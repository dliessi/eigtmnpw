function ff = barint(x, w, f, xx)
%BARINT Barycentric interpolation
%  ff = BARINT(x, w, f, xx) computes the values ff of a function at xx
%  using the barycentric interpolation formula with x interpolation
%  nodes, w barycentric weights and f values of the function at x.
%
%  Reference:
%    J.-P. Berrut and L. N. Trefethen,
%    Barycentric Lagrange interpolation,
%    SIAM Review, 46(3):501-517, 2004,
%    DOI: 10.1137/S0036144502417715

n = length(x);
numer = zeros(size(xx));
denom = zeros(size(xx));
exact = zeros(size(xx));
for j = 1:n
    xdiff = xx - x(j);
    temp = w(j) ./ xdiff;
    numer = numer + temp * f(j);
    denom = denom + temp;
    exact(xdiff == 0) = j;
end
jj = find(exact);
ff = numer ./ denom;
ff(jj) = f(exact(jj));
end
