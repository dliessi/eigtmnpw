function w = barwei(x)
%BARWEI Barycentric weights
%  w = BARWEI(x) returns the weights w for barycentric interpolation
%  at the nodes x, normalized to 1.
%
%  Reference:
%    J.-P. Berrut and L. N. Trefethen,
%    Barycentric Lagrange interpolation,
%    SIAM Review, 46(3):501-517, 2004,
%    DOI: 10.1137/S0036144502417715

n = length(x);
dx = ones(n, 1);
w = dx;
for j = 2:n
    for k = 1:j-1
        dx(k) = x(j) - x(k);
        w(k) = - dx(k) * w(k);
    end
    w(j) = prod(dx(1:j));
end
w = 1 ./ w;
w = w / norm(w, Inf);
end
