function f = make_num_per_sol_db(sol)
% make_num_per_sol_db: given a numerical periodic solution of a DDE as a
%   point structure sol of kind 'psol' computed by DDE-BIFTOOL, build the
%   corresponding piecewise polynomial and return its function handle
%   f(i, t), with i the index of the component of the solution and t a
%   vector of points to evaluate
% f = make_num_per_sol_db(sol)

assert(isequal('psol', sol.kind))

[d, mesh_length] = size(sol.profile);
L = (mesh_length-1) / sol.degree;
mesh = sol.mesh(1:sol.degree:end);

coefficients = zeros(L, sol.degree+1, d);
for i = 1:d
  for j = 1:L
    coefficients(j, :, i) = ...
      polyfit(linspace(mesh(j), mesh(j+1), sol.degree+1)-mesh(j), ...
              sol.profile(i, (((j-1)*sol.degree+1):(j*sol.degree+1))), ...
              sol.degree);
  end
end

polynomials = cell(d, 1);
for i = 1:d
  polynomials{i} = mkpp(mesh, coefficients(:, :, i));
end

f = @(i, t) ppval(polynomials{i}, t/sol.period-floor(t/sol.period));

end
