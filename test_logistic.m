%% Logistic DDE
% x'(t) = r * x(t) * (1-x(t-1))

%% Define the system

load test_logistic_sol_db.mat
sol_db.par.sol = sol_db.sol;

% The linearized equation around a solution xbar reads
% x'(t) = r * (1-xbar(t-1)) * x(t) - r * xbar(t) * x(t-1)

system = eigTMNpw_system(...
    [0 1], ... % dimensions [dX dY]
    sol_db.par.tau, ... % delays
    sol_db.period, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'AYY', @(t, par) par.r * (1-par.sol(1, t-1)), ...
    'BYY', { @(t, par) - par.r * par.sol(1, t) }, ...
    'par', sol_db.par, ...
    't', sol_db.mesh ...
    );

%% Compute the spectrum of the monodromy operator (Floquet multipliers)

method = eigTMNpw_method(sol_db.ncol);
mult = eigTMNpw(system, method, 'verbosity', 'info');

%% Plot results

figure
plot_multipliers(mult)
title(sprintf('\\mu_1 = %.4f, err = %.4e', mult(1), abs(mult(1)-1)))
