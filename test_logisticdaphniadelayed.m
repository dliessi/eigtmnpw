%% Simplified delayed logistic Daphnia
% b(t) = beta * S(t) * int_abar^tau b(t-a) da
% S'(t) = r * S(t) * (1 - S(t-tau)/K) - gamma * S(t) * int_abar^tau b(t-a) da

%% Define the system

load test_logisticdaphniadelayed_sol_mc.mat

parnames = {'beta', 'abar', 'r', 'K', 'gamma', 'aux', 'tau', 'M'};
par = [parnames; num2cell(slc.data.parametervalues')];
par = struct(par{:});
par.sol = make_num_per_sol_mc(dX, dY, xlc, slc, flc);
mesh= flc(1:slc.data.ntst+1);
period = xlc(end-1);

% The linearized equation around a solution (bbar, Sbar) reads
% b(t) = beta * Sbar(t) * int_abar^tau b(t-a) da + beta * int_abar^tau bbar(t-a)da * S(t)
% S'(t) = (r * (1 - Sbar(t-tau) / K) - gamma * int_abar^tau bbar(t-a) da) * S(t)
%         - r / K * Sbar(t) * S(t-tau) - gamma * Sbar(t) * int_abar^tau b(t-a) da

system = eigTMNpw_system(...
  [dX, dY], ...            % dimensions [dX, dY]
  [par.abar, par.tau], ... % delays
  period, ...              % period (or length of time evolution)
  0, ...                   % initial time
  'AXY', @(t, par) par.beta ...
         * integral(@(s) par.sol(1, t-s), par.abar, par.tau), ...
  'AYY', @(t, par) par.r * (1 - par.sol(2, t-par.tau) / par.K) ...
         - par.gamma ...
         * integral(@(s) par.sol(1, t-s), par.abar, par.tau), ...
  'BYY', {[], @(t, par) -par.r / par.K * par.sol(2, t)}, ...
  'CXX', {[], @(t, theta, par) par.beta * par.sol(2, t)}, ...
  'CYX', {[], @(t, theta, par) -par.gamma * par.sol(2, t)}, ...
  'par', par, ...
  't', mesh);

%% Compute the spectrum of the monodromy operator (Floquet multipliers)

method = eigTMNpw_method(slc.data.ncol);
mult = eigTMNpw(system, method);

%% Plot results

figure
plot_multipliers(mult)
title(sprintf('\\mu_1 = %.4f, err = %.4e', mult(1), abs(mult(1)-1)))

figure
ttt = linspace(0, period, 100);
plot(ttt, par.sol(1, ttt))
hold on
plot(ttt, par.sol(2, ttt))
plot(mesh*period, ones(size(mesh))*0.27, '.')
