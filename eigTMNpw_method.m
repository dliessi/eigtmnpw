function method = eigTMNpw_method(varargin)
% eigTMNpw_method: build struct of method parameters
% method = eigTMNpw_method(varargin)
%
% Optional positional parameters
% (in MATLAB they can be specified also as named parameters)
%   M: degree of collocation polynomials (default: 10)
%
% Named parameters
%   CollocationFamily: name of family of collocation points (default: 'cheb2')
%     supported families:
%       - 'cheb2': Chebyshev type II (extrema)
%       - 'cheb1e': Chebyshev type I (zeros) plus interval endpoints
%       - 'equi': equidistant points
%       - 'gaussleg': Gauss-Legendre points plus interval endpoints
%   QuadratureMethod: name of quadrature method (default: 'clencurt')
%     supported methods:
%       - 'clencurt': Clenshaw-Curtis (as in [Trefethen2008])
%           N.B. if max(dX, dY) = 1, vectorize the C** coefficients of the
%           system with respect to theta and set 'Vectorized' to true (see
%           below) to take advantage of the vectorization
%       - 'quad': MATLAB's quad (disabled in Octave)
%       - 'integral': MATLAB/Octave's integral
%           N.B. if max(dX, dY) = 1, vectorize the C** coefficients of the
%           system with respect to theta and set 'ArrayValued' to false (see
%           below) to take advantage of the vectorization
%   QuadratureParameters: parameters as required by the chosen quadrature method
%     parameters for supported methods:
%       - 'clencurt':  cell array containing the optional named parameters
%           'Degree' (degree of precision, default 10), 'Vectorized' (compute
%           all function values in one call, for use when max(dX, dY) = 1,
%           default false)
%       - 'quad': tolerance, scalar (see quad's help)
%       - 'integral': cell array containing the optional named parameters
%           'RelTol', 'AbsTol' and 'ArrayValued' (see integral's help)
%   Threshold: threshold on interval length relative to Omega (default: eps)
%     - if the length of [-Tau, 0] relative to Omega is below Threshold, stop
%     - if the length of the smallest piece of [-Tau, 0] relative to Omega is
%       below Threshold, act according to Strategy
%   Strategy: strategy for dealing with the last piece in [-Tau, 0] if it is
%     cropped (default: 2)
%     - 1: enlarge the delay interval to match the full last piece
%     - 2: always reinterpolate the cropped last piece
%     - 3: like 2, but discard the cropped last piece if it is too small
%     - 4: like 2, but if the cropped last piece is too small reinterpolate the
%          union of the two last pieces
%   ZeroDirection: direction of the inclusion of 0 (default: 1)
%     - -1: 0 is included in the backward interval,
%           i.e. [-Tau, 0)] = [-Tau, 0] and [(0, Omega] = (0, Omega]
%     -  1: 0 is included in the forward interval,
%           i.e. [-Tau, 0)] = [-Tau, 0) and [(0, Omega] = [0, Omega]
%
% Output
%   method: a struct with the following fields
%     - M, Threshold, Strategy, ZeroDirection: as input
%     - c: collocation points in [0, 1] (in ascending order)
%     - bw: barycentric weights corresponding to c
%     - qw, qx: quadrature weights and nodes (ideally they match c, so that qx
%         equals c and qw define the corresponding interpolatory quadrature
%         formula, but they may be different)
%
% eigTMNpw, version 0.4, 2022-09-29
% Author: Davide LIESSI <name.surname@uniud.it>
%         Department of Mathematics, Computer Science and Physics
%         University of Udine, Italy
% Copyright (c) 2020-2022 Davide Liessi
% This file is distributed under the terms of the MIT license (see LICENSE.txt).

parser = inputParser;

defaultM = 10;
checkM = @(x) isnumeric(x) && isreal(x) && isscalar(x) ...
              && (floor(x) == ceil(x)) && (x >= 1);
defaultCollocationFamily = 'cheb2';
validCollocationFamilies = {'cheb2', 'cheb1e', 'equi', 'gaussleg'};
checkCollocationFamily = @(x) any(validatestring(x, validCollocationFamilies));
defaultQuadratureMethod = 'clencurt';
validQuadratureMethods = {'clencurt', 'quad', 'integral'};
checkQuadratureMethod = @(x) any(validatestring(x, validQuadratureMethods));
defaultThreshold = eps;
checkThreshold = @(x) isnumeric(x) && isreal(x) && isscalar(x) && (x > 0);
checkTol = checkThreshold;
defaultStrategy = 2;
checkStrategy = @(x) isnumeric(x) && isscalar(x) && any(x == 1:4);
defaultZeroDirection = 1;
checkZeroDirection = @(x) isnumeric(x) && isscalar(x) && any(x == [1, -1]);
checkBool = @(x) isscalar(x) && (islogical(x) || (isnumeric(x) && (x == 0 || x == 1)));

addOptional(parser, 'M', defaultM, checkM)
addParameter(parser, 'CollocationFamily', ...
            defaultCollocationFamily, checkCollocationFamily)
addParameter(parser, 'QuadratureMethod', ...
            defaultQuadratureMethod, checkQuadratureMethod)
addParameter(parser, 'QuadratureParameters', [])
addParameter(parser, 'Threshold', defaultThreshold, checkThreshold)
addParameter(parser, 'Strategy', defaultStrategy, checkStrategy)
addParameter(parser, 'ZeroDirection', defaultZeroDirection, checkZeroDirection)

parse(parser, varargin{:})



method.M = parser.Results.M;
method.Threshold = parser.Results.Threshold;
method.Strategy = parser.Results.Strategy;
method.ZeroDirection = parser.Results.ZeroDirection;

method.CollocationFamily = parser.Results.CollocationFamily;
switch method.CollocationFamily
  case 'cheb2'
    % Chebyshev type II (extrema) points in [0, 1] (in ascending order)
    method.c = (1 - cos((0:method.M)*pi/method.M)) / 2;
    % and corresponding barycentric weights
    method.bw = [1/2, ones(1, method.M-1), 1/2] .* (-1) .^ (0:method.M);
  case 'cheb1e'
    % Chebyshev type I (zeros) points in [0, 1] (in ascending order)
    % including interval endpoints
    method.c = [0, (1 - cos((2*(0:(method.M-2))+1)*pi/(2*(method.M-2)+2))) / 2, 1];
    % and corresponding barycentric weights
    method.bw = barwei(method.c);
  case 'equi'
    % Equidistant points in [0, 1] (in ascending order)
    method.c = linspace(0, 1, method.M+1);
    % and corresponding barycentric weights
    method.bw = barwei(method.c);
  case 'gaussleg'
    % Gauss-Legendre points in [0, 1] (in ascending order)
    % including interval endpoints
    method.c = [0, lgwt(method.M-1, 1, 0)', 1];
    % and corresponding barycentric weights
    method.bw = barwei(method.c);
end

method.QuadratureMethod = parser.Results.QuadratureMethod;
switch method.QuadratureMethod
  case 'clencurt'
    qM = 10;
    vect = false;
    if ~isempty(parser.Results.QuadratureParameters)
      qparser = inputParser;
      addParameter(qparser, 'Degree', [], checkM)
      addParameter(qparser, 'Vectorized', false, checkBool)
      parse(qparser, parser.Results.QuadratureParameters{:})
      if ~isempty(qparser.Results.Degree)
        qM = qparser.Results.Degree;
      end
      if ~isempty(qparser.Results.Vectorized)
        vect = qparser.Results.Vectorized;
      end
    end
    % Chebyshev type II (extrema) points in [-1, 1] (in descending order)
    % and Clenshaw-Curtis quadrature weights
    [qw, qx] = clencurt(qM);
    method.quad = @(f, a, b) clencurtquad(f, a, b, qM, qx, qw, vect);
  case 'quad'
    assert(~uioctave)
    if ~isempty(parser.Results.QuadratureParameters)
      qtol = parser.Results.QuadratureParameters;
      assert(checkTol(qtol))
      method.quad = @(f, a, b) quad(f, a, b, qtol);
    else
      method.quad = @quad;
    end
  case 'integral'
    if ~isempty(parser.Results.QuadratureParameters)
      qparser = inputParser;
      addParameter(qparser, 'RelTol', [], checkTol)
      addParameter(qparser, 'AbsTol', [], checkTol)
      addParameter(qparser, 'ArrayValued', true, checkBool)
      parse(qparser, parser.Results.QuadratureParameters{:})
      integralParameters = {};
      if ~isempty(qparser.Results.RelTol)
        integralParameters = [integralParameters, {'RelTol', qparser.Results.RelTol}];
      end
      if ~isempty(qparser.Results.AbsTol)
        integralParameters = [integralParameters, {'AbsTol', qparser.Results.AbsTol}];
      end
      if ~isempty(qparser.Results.ArrayValued)
        integralParameters = [integralParameters, {'ArrayValued', qparser.Results.ArrayValued}];
      end
      method.quad = @(f, a, b) integral(f, a, b, integralParameters{:});
    else
      method.quad = @(f, a, b) integral(f, a, b, 'ArrayValued', true);
    end
end

% Integrals of Lagrange coefficients on [0, 1]
IM = eye(method.M+1);
method.intlj = zeros(method.M+1, 1);
for j = 0:method.M
  method.intlj(j+1) = method.quad(@(t) barint(method.c, method.bw, IM(:, j+1), t), 0, 1);
end

assert(isnumeric(method.c) && isreal(method.c) && isvector(method.c))
assert(isequal(method.c, unique(method.c)))
assert(method.c(1) == 0 && method.c(end) == 1)
if ~isrow(method.c)
  method.c = method.c';
end
assert(isnumeric(method.bw) && isreal(method.bw) && isvector(method.bw))

end


% TODO: comment and checks
function out = clencurtquad(f, a, b, qM, qx, qw, vect)
out = 0;
if a == b
  return
end
qx = (qx+1) * (b-a) / 2 + a;
if vect
  out = dot(qw, f(qx));
else
  for i = 1:(qM+1)
    out = out + qw(i) * f(qx(i));
  end
end
out = (b-a) / 2 * out;
end
