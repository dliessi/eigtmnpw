%% A renewal equation with quadratic nonlinearity
% x(t) = gamma / 2 int_{-3}^{-1} x(t+theta) (1-x(t+theta)) d theta
% and a known periodic solution (for gamma >= 2+pi/2; period 4)
% xbar(t) = 1/2 + pi/(4 gamma) + sqrt(1/2 - 1/gamma - pi/(2 gamma^2)*(1+pi/4)) sin(pi/2 t)

%% Define the system

xbar = @(t, gamma) 1/2 + pi/(4*gamma) + sqrt(1/2 - 1/gamma - pi/(2*gamma^2)*(1+pi/4)) * sin(pi/2*t);

% The linearized equation around a solution xbar reads
% x(t) = gamma / 2 int_{-3}^{-1} (1 - 2 xbar(t+theta)) x(t+theta) d theta

system = eigTMNpw_system(...
    [1 0], ... % dimensions [dX dY]
    [1 3], ... % delays
    4, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'CXX', {[], @(t, theta, par) par/2 * (1 - 2 * xbar(t+theta, par))});

%% Find the Hopf bifurcation using fzero

fprintf('\ngamma = %.15f\n\n', fzero(@(gamma) test_quadraticre_bif(gamma, system), 4))

%% Find the first period doubling via bisection

tol = 1e-12;
maxk = 50;
a = 3.6;
b = 5;

rho1_a = test_quadraticre_bif(a, system);
rho1_b = test_quadraticre_bif(b, system);

k = 0;
while k < maxk
    x = (a + b) / 2;
    rho1_x = test_quadraticre_bif(x, system);
    err = abs(b - x);
    fprintf('k = %d, err = %d\n', k, err)
    if rho1_x == 0
        err = 0;
        break
    end
    if err < tol
        break
    end
    if rho1_a * rho1_x > 0
        a = x;
        rho1_a = rho1_x;
    else
        b = x;
    end
    k = k + 1;
end
fprintf('\ngamma = %.15f\n\n', x)
