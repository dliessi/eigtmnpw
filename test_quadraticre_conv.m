%% A renewal equation with quadratic nonlinearity
% x(t) = gamma / 2 int_{-3}^{-1} x(t+theta) (1-x(t+theta)) d theta
% and a known periodic solution (for gamma >= 2+pi/2; period 4)
% xbar(t) = 1/2 + pi/(4 gamma) + sqrt(1/2 - 1/gamma - pi/(2 gamma^2)*(1+pi/4)) sin(pi/2 t)

%% Define the system

xbar = @(t, gamma) 1/2 + pi/(4*gamma) + sqrt(1/2 - 1/gamma - pi/(2*gamma^2)*(1+pi/4)) * sin(pi/2*t);

% The linearized equation around a solution xbar reads
% x(t) = gamma / 2 int_{-3}^{-1} (1 - 2 xbar(t+theta)) x(t+theta) d theta

system = @(L) eigTMNpw_system(...
    [1 0], ... % dimensions [dX dY]
    [1 3], ... % delays
    4, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'CXX', {[], @(t, theta, par) par/2 * (1 - 2 * xbar(t+theta, par))}, ...
    'par', 4, ...
    'L', L ...
    );

%% Compute reference value with L = 40, m = 15

fprintf('Computing the reference multipliers (it takes a while: set the ''Verbosity'' option to ''verbose'' to see the progress)\n')
mult_ref = eigTMNpw(system(40), eigTMNpw_method(15));

%% Convergence of the FEM (m = 3, L increasing)

method3 = eigTMNpw_method(3);
mult_fem = cell(40, 1);
fprintf('Computing the multipliers with m = 3 and L = \n')
for L = 1:40
    fprintf('%d, ', L)
    mult_fem{L} = eigTMNpw(system(L), method3);
    if mod(L, 10) == 0
        fprintf('\n')
    end
end

%% Plot

figure
loglog(1:40, cellfun(@(x) min(abs(x-1)), mult_fem), '.-k', 'MarkerSize', 15, 'DisplayName', '\mu_{1}')
hold on
loglog(1:40, cellfun(@(x) min(abs(x-mult_ref(2))), mult_fem), '*-k', 'MarkerSize', 7, 'DisplayName', '\mu_{2}')
xlabel('L')
title('FEM, m = 3')
legend('Location', 'northeast')

%% Convergence of the SEM (L = 20, m increasing)

system20 = system(20);
mult_sem = cell(10, 1);
fprintf('Computing the multipliers with L = 20 and m = \n')
for m = 1:10
    fprintf('%d, ', m)
    mult_sem{m} = eigTMNpw(system20, eigTMNpw_method(m));
end
fprintf('\n')

%% Plot

figure
loglog(1:10, cellfun(@(x) min(abs(x-1)), mult_sem), '.-k', 'MarkerSize', 15, 'DisplayName', '\mu_{1}')
hold on
loglog(1:10, cellfun(@(x) min(abs(x-mult_ref(2))), mult_sem), '*-k', 'MarkerSize', 7, 'DisplayName', '\mu_{2}')
xlabel('L')
title('SEM, L = 20')
legend('Location', 'northeast')
