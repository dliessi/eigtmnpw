function plot_multipliers(mu)
%PLOT_MULTIPLIERS(mu) plots the complex numbers mu and the unit circle

plot(real(mu), imag(mu), '.', 'MarkerSize', 30)
hold on
x = linspace(-1, 1);
plot(x, sqrt(1 - x.^2), 'r')
plot(x, -sqrt(1 - x.^2), 'r')
axis equal
hold off

end
