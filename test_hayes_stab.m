function test = test_hayes_stab(a, b, system)
system.par = [a, b];
mult = eigTMNpw(system);
test = max(abs(mult)) - 1;
end
