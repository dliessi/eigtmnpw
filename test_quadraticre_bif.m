function test = test_quadraticre_bif(gamma, system)
system.par = gamma;
mult = eigTMNpw(system);
% ignore closest to 1 (1 is always present due to linearization)
[~, ind] = min(abs(mult-1));
mult(ind) = [];
test = prod(abs(mult)-1);
end
