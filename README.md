eigTMNpw
=======

Approximate the spectrum of evolution operators of systems of coupled renewal equations and delay differential equations.

If you use this software for a scientific publication, please cite the following publications.
* D. Breda, D. Liessi, and R. Vermiglio, Piecewise discretization of monodromy operators of delay equations on adapted meshes, J. Comput. Dyn., to appear.
* D. Breda, D. Liessi, and R. Vermiglio, A practical guide to piecewise pseudospectral collocation for Floquet multipliers of delay equations in MATLAB, submitted.


Copyright and licensing
-----------------------

### eigTMNpw

Copyright (c) 2020-2022 Davide Liessi

eigTMNpw is distributed under the terms of the MIT license (see `LICENSE.txt`).


### barint.m, barwei.m

Copyright (c) 2004 Jean-Paul Berrut, Lloyd N. Trefethen

The code in barint.m and barwei.m is taken, with few changes, from
J.-P. Berrut and L. N. Trefethen,
Barycentric Lagrange interpolation,
SIAM Review, 46(3):501-517, 2004,
DOI: 10.1137/S0036144502417715.

Even though the codes therein are not explicitly licensed, considering also that variations of them are included in [Chebfun](http://www.chebfun.org/), which is distributed under the 3-clause BSD license, I believe that the authors’ intention was for their codes to be freely used and that distributing these files together with eigTMNpw does not violate their rights.


### clencurt.m

Copyright (c) 2000 Lloyd N. Trefethen

The code in clencurt.m is taken from
L. N. Trefethen,
Spectral Methods in MATLAB,
SIAM, 2000,
DOI: 10.1137/1.9780898719598.

Even though the codes therein are not explicitly licensed, considering that they are made available for download on the author’s web page and that variations of some of them are included in [Chebfun](http://www.chebfun.org/), which is distributed under the 3-clause BSD license, I believe that the author’s intention was for his codes to be freely used and that distributing this file together with eigTMNpw does not violate his rights.


### lgwt.m

lgwt, version 1.0.0.0, 2004-05-11, by Greg von Winckel

Copyright (c) 2009 Greg von Winckel

Retrieved on 2021-07-05 from
https://www.mathworks.com/matlabcentral/fileexchange/4540-legendre-gauss-quadrature-weights-and-nodes

lgwt is distributed under the terms of the 2-clause BSD license (see `private/lgwt-license.txt`).


### verbosity.m

verbosity, version 1.0.0, 2018-12-06, by Emmanuel Farhi

Copyright (c) 2018 Emmanuel Farhi

Retrieved on 2020-10-15 from
https://www.mathworks.com/matlabcentral/fileexchange/69631-verbosity

verbosity is distributed under the terms of the 3-clause BSD license (see `private/verbosity-license.txt`).
