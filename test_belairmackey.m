%% Belair-Mackey platelet model
% x'(t) = - gamma * x(t) + q(x(t-tau_m)) - q(x(t-tau_m-tau_s)) * exp(-gamma*tau_s)
% with q(x) = q_0 * theta^n * x / (theta^n + x^n)

%% Define the system

load test_belairmackey_sol_db.mat

parnames = {'gamma', 'q0', 'n', 'theta', 'tau_m', 'tau_s', 'tau_ms'};
par = cell2struct(num2cell(sol.parameter), parnames, 2);
par.sol = make_num_per_sol_db(sol);
mesh = sol.mesh(1:sol.degree:end);

% The linearized equation around a solution xbar reads
% x'(t) = - gamma * x(t) + q'(xbar(t-tau_m)) * x(t-tau_m)
%         - q'(xbar(t-tau_m-tau_s)) * exp(-gamma*tau_s) * x(t-tau_m-tau_s)

dq = @(x, q0, n, theta) q0 * theta^n * ((1-n) * x.^n + theta^n) ./ (theta^n + x.^n)^2;
system = eigTMNpw_system(...
    [0, 1], ...
    [par.tau_m, par.tau_ms], ...
    sol.period, ...
    0, ...
    'AYY', @(t, par) -par.gamma, ...
    'BYY', {@(t, par) dq(par.sol(1, t-par.tau_m), par.q0, par.n, par.theta), ...
            @(t, par) dq(par.sol(1, t-par.tau_ms), par.q0, par.n, par.theta) ...
                      * exp(-par.gamma*par.tau_s)}, ...
    'par', par, ...
    't', mesh);

%% Compute the spectrum of the monodromy operator (Floquet multipliers)

method = eigTMNpw_method(sol.degree);
mult = eigTMNpw(system, method);

%% Plot results

figure
plot_multipliers(mult)
title(sprintf('\\mu_1 = %.4f, err = %.4e', mult(1), abs(mult(1)-1)))

figure
ttt = linspace(0, sol.period, 100);
plot(ttt, par.sol(1, ttt))
hold on
plot(mesh*sol.period, ones(size(mesh))*20, '.')
