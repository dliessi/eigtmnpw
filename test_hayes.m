%% Hayes delay differential equation
% x'(t) = a x(t) + b x(t-1)

%% Setup path for LEVEL

addpath('~/lib/matlab/level');

%% Define the system

system = eigTMNpw_system(...
    [0 1], ... % dimensions [dX dY]
    1, ... % delays
    1, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'AYY', @(t, par) par(1), ...
    'BYY', { @(t, par) par(2) } ...
    );

%% Compute the spectrum for some notable values of the parameters

b = -pi/2;
avalues = [-1, 0, 1, 1+log(pi/2), 1.5, pi/2, 2];
avaluestext = { '-1', '0', '1', '1+log(\pi/2)', '1.5', '\pi/2', '2' };
muexpectedtext = { ...
    '|\mu_{1,2}| < 1', ...
    '\mu_{1,2} = \pm{}i', ...
    '|\mu_{1,2}| > 1', ...
    '\mu_1 = \mu_2 = \pi/2', ...
    '\mu_1 > 1, \mu_2 > 1', ...
    '\mu_1 > 1, \mu_2 = 1', ...
    '\mu_1 > 1, 0 < \mu_2 < 1' };
system.par = [0, b];
figure
fprintf('b = -pi/2\n')
for i = 1:length(avalues)
    fprintf('i = %d, a = %12s', i, avaluestext{i})
    system.par(1) = avalues(i);
    mu = eigTMNpw(system);
    fprintf(', mu_1 = %+.4f%+.4fi, mu_2 = %+.4f%+.4fi\n', ...
        real(mu(1)), imag(mu(1)), real(mu(2)), imag(mu(2)))
    if i <= 3
        subplot(2, 4, i)
    else
        subplot(2, 4, i+1)
    end
    plot_multipliers(mu)
    title(sprintf('a = %s, %s', avaluestext{i}, muexpectedtext{i}))
    drawnow
end
fprintf('\n')

%% Produce a rough stability chart

N = 15;
domain = [-2, 2, -2, 2];
avalues = linspace(domain(1), domain(2), N);
bvalues = linspace(domain(3), domain(4), N);

mu = cell(length(avalues), length(bvalues));

figure
for i = 1:length(avalues)
    fprintf('i = %2d, j =', i)
    for j = 1:length(bvalues)
        fprintf(' %d', j)
        system.par = [avalues(i), bvalues(j)];
        mu{i,j} = eigTMNpw(system);
        if all(abs(mu{i,j}) < 1)
            plot(avalues(i), bvalues(j), 'b.', 'MarkerSize', 10)
        else
            plot(avalues(i), bvalues(j), 'rx', 'MarkerSize', 10)
        end
        hold on
        axis(domain)
        drawnow
    end
    fprintf('\n')
end
fprintf('\n')

%% Produce a refined stability chart with LEVEL

domain = [-2, 2, -2, 2];
TOL = 1e-1;
% level 0 poses some numerical problems: using 1e-3 instead
% levels 1e-3 and -1e-3 are indistinguishable with the naked eye
level(@(a, b) test_hayes_stab(a, b, system), 1e-3, domain, TOL);
