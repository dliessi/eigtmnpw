%% Hayes delay differential equation
% x'(t) = (1-|mod(t,2)-1|) x(t-1)

%% Define the system

a = @(t) 1-abs(mod(t,2)-1);
system_1p = eigTMNpw_system(...
    [0 1], ... % dimensions [dX dY]
    1, ... % delays
    2, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'BYY', { @(t, par) a(t) } ...
    );
system_pw = eigTMNpw_system(...
    [0 1], ... % dimensions [dX dY]
    1, ... % delays
    2, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'BYY', { @(t, par) a(t) }, ...
    't', [0 1 2] ...
    );

%% Test the convergence of the non-piecewise and piecewise method

Mmax = 60;

mult_1p = cell(Mmax, 1);
mult_pw = cell(Mmax, 1);
fprintf('M =')
for M = 1:Mmax
    fprintf (' %d', M)
    mult_1p{M} = eigTMNpw(system_1p, eigTMNpw_method(M));
    mult_pw{M} = eigTMNpw(system_pw, eigTMNpw_method(M));
end
fprintf('\n')
mult_ref = eigTMNpw(system_pw, eigTMNpw_method(2*Mmax), 'Verbosity', 'verbose');

err_1p = cellfun(@(x) abs(x(1)-mult_ref(1)), mult_1p);
err_pw = cellfun(@(x) abs(x(1)-mult_ref(1)), mult_pw);

figure
loglog(1:Mmax, err_1p, 'DisplayName', 'L = 1')
hold on
loglog(1:Mmax, err_pw, 'DisplayName', 'L = 2')
xlabel('M')
title(sprintf('Error with respect to M=%d', 2*Mmax))
legend('Location', 'southwest')
