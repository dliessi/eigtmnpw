%% Simplified delayed logistic Daphnia
% b(t) = beta * S(t) * int_abar^tau b(t-a) da
% S'(t) = r * S(t) * (1 - S(t-tau)/K) - gamma * S(t) * int_abar^tau b(t-a) da

%% Compute periodic solution with MatCont and the method of [1]

% This file is inspired by a script courtesy of Francesca Scarabel.

% [1] D. Breda, O. Diekmann, M. Gyllenberg, F. Scarabel and R. Vermiglio,
%     Pseudospectral discretization of nonlinear delay equations: New prospects
%     for numerical bifurcation analysis, SIAM J. Appl. Dyn. Syst., 15 (2016),
%     pp. 1–23, DOI: 10.1137/15M1040931

%% Setup MatCont and clear workspace

mcroot = '~/lib/matlab/MatCont/7.2';
oldwd = cd(mcroot);
init
cd(oldwd)

clear
clear global

global cds;

%% Define model and parameters

% Dimensions of the RE and DDE, respectively
dX = 1;
dY = 1;

% The model is defined in the file test_logisticdaphniadelayed_sol_mc_system.m,
% courtesy of Francesca Scarabel.
handles = feval(@test_logisticdaphniadelayed_sol_mc_system);

% Initial parameter values
beta = 2;
abar = 3;
r = 1;
K = 1;
gamma = 1;
tau = 4;

% Discretization parameter
M = 20;

% Dimension of the approximating ODE system
MM = dX * M + dY * (M+1);

% Auxiliary parameter, unused in this file
aux = 1;

par = [beta, abar, r, K, gamma, aux, tau, M]';
parnames = {'beta', 'abar', 'r', 'K', 'gamma', 'aux', 'tau', 'M'};

% Index of continuation parameter
contpar = 3;

% Nontrivial equilibrium corresponding to par
beq = r / (gamma * (tau-abar)) * (1 - 1 / (K * beta * (tau-abar)));
Seq = 1 / (beta * (tau-abar));
% Trivial equilibria: (0, 0) and (0, K)

%% Equilibrium continuation

fprintf('Starting equilibrium continuation from initial point (b, S) = (%.4f, %.4f)\n', beq, Seq);

opt = contset;
opt = contset(opt, 'Singularities', 1);
opt = contset(opt, 'MaxNumPoints', 40);
opt = contset(opt, 'Backward', 1);

% Bound on continuation parameter
contparbound = 4;

% Initialize equilibrium vector
Weq = feval(handles{1}, M, beq, Seq);
[x0, v0] = init_EP_EP(@test_logisticdaphniadelayed_sol_mc_system, Weq, par, contpar);

% Continue branch until there is at least one special point
[xe, ve, se, he, fe] = cont(@equilibrium, x0, v0, opt);
fprintf('Current value of %s = %.4f\n', parnames{contpar}, xe(end,end))
while ((length(se) < 3) && xe(end,end) < contparbound)
    [xe, ve, se, he, fe] = cont(xe, ve, se, he, fe, cds);
    fprintf('Current value of %s = %.4f\n', parnames{contpar}, xe(end,end))
end

% Plot equilibrium branch and special points
fh = figure;
cpl(xe, ve, se, [MM+1, 1]);
hold on;
xlabel(parnames{contpar})
ylabel('b')

%% Detection of singular points

% Detect first Hopf (H) point
for ii = 1:size(se)
    if strcmp(se(ii).label, 'H ') == 1
        H_index = se(ii).index;
        break
    end
end
H = xe(1:MM, H_index);
par_H = par;
par_H(contpar) = xe(end, H_index);

fprintf('First Hopf bifurcation at %s = %.4f\n', parnames{contpar}, par_H(contpar));

%% Limit cycle continuation from H

fprintf('Starting LC continuation from H\n');

opt = contset;
UserInfo.name = 'userf';
UserInfo.state = 1;
UserInfo.label = 'P ';
opt = contset(opt, 'Userfunctions', 1);
opt = contset(opt, 'UserfunctionsInfo', UserInfo);
opt = contset(opt, 'Multipliers', 1);
opt = contset(opt, 'MaxStepsize', 1);
opt = contset(opt, 'MaxNumPoints', 5);

% Bound on continuation parameter (little after the value detected by userf)
contparbound = 0.305;

% Discretization parameters for the limit cycle
% ntst: number of pieces in the period interval
% ncol: degree of polynomial
ntst = 40;
ncol = 4;

% Initialize limit cycle
[x0, v0] = init_H_LC(@test_logisticdaphniadelayed_sol_mc_system, ...
    H, par_H, contpar, 1e-2, ntst, ncol);

% Continue branch
jj = 1;
fprintf('Batch %d\n', jj)
[xlc, vlc, slc, hlc, flc] = cont(@limitcycle, x0, v0, opt);
fprintf('Current value of %s = %.4f\n', parnames{contpar}, xlc(end,end))
while (xlc(end, end) < contparbound && jj < 30)
    jj = jj + 1;
    fprintf('Batch %d\n', jj)
    [xlc, vlc, slc, hlc, flc] = cont(xlc, vlc, slc, hlc, flc, cds);
    fprintf('Current value of %s = %.4f\n', parnames{contpar}, xlc(end,end))
end

% Plot max and min of periodic solutions
max_LC = max(xlc(1:MM:((ntst*ncol+1)*MM), :));
min_LC = min(xlc(1:MM:((ntst*ncol+1)*MM), :));
figure(fh)
plot(xlc(end, :), max_LC, 'g', xlc(end, :), min_LC, 'g');

% Save limit cycle detected by userf (r = 0.3)
slc = slc(2);
xlc = xlc(:, slc.index);
vlc = vlc(:, slc.index);
hlc = hlc(:, slc.index);
flc = flc(:, slc.index);

save test_logisticdaphniadelayed_sol_mc.mat dX dY xlc vlc slc hlc flc
