%% Plant neural model, coupled DDE and RE version
% v'(t) = v(t) - v(t)^3/3 - w(t) + mu * (v(t-tau) - v_0)
% w(t) = w(t-tau) + int_{t-tau}^t rho * (v(s) + a - b * w(s)) ds
% with v_0 a real root of v - v^3/3 - (v + a) / b

%% Define the system

% Use a periodic solution of the original Plant neural model.
load test_plantneural_sol_db.mat
sol_db.par.sol = sol_db.sol;

% The linearized equation around a solution (vbar, wbar) reads
% v'(t) = (1 - vbar(t)^2) * v(t) - w(t) + mu * v(t-tau)
% w(t) = w(t-tau) + rho * int_{t-tau}^t v(s) ds - rho * b * int_{t-tau}^t w(s) ds

system = eigTMNpw_system(...
    [1 1], ... % dimensions [dX dY]
    sol_db.par.tau, ... % delays
    sol_db.period, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'AYY', @(t, par) 1-par.sol(1, t).^2, ...
    'AYX', @(t, par) -1, ...
    'BYY', { @(t, par) par.mu }, ...
    'BXX', { @(t, par) 1 }, ...
    'CXY', { @(t, theta, par) par.rho .* ones(size(theta)) }, ...
    'CXX', { @(t, theta, par) -par.b*par.rho .* ones(size(theta)) }, ...
    'par', sol_db.par, ...
    't', sol_db.mesh ...
    );

%% Compute the spectrum of the monodromy operator (Floquet multipliers)

method = eigTMNpw_method(sol_db.ncol, 'QuadratureParameters', {'Vectorized', true});
mult = eigTMNpw(system, method, 'verbosity', 'verbose');

%% Plot results

figure
plot_multipliers(mult)
title(sprintf('\\mu_1 = %.4f, err = %.4e', mult(1), min(abs(mult-1))))
