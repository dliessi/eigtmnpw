%% Plant neural model
% v'(t) = v(t) - v(t)^3/3 - w(t) + mu * (v(t-tau) - v_0)
% w'(t) = rho * (v(t) + a - b * w(t))
% with v_0 a real root of v - v^3/3 - (v + a) / b

%% Define the system

load test_plantneural_sol_db.mat
sol_db.par.sol = sol_db.sol;

% The linearized equation around a solution (vbar, wbar) reads
% v'(t) = (1 - vbar(t)^2) * v(t) - w(t) + mu * v(t-tau)
% w'(t) = rho * v(t) - b * rho * w(t)

system = eigTMNpw_system(...
    [0 2], ... % dimensions [dX dY]
    sol_db.par.tau, ... % delays
    sol_db.period, ... % period (or length of time interval of the evolution operator)
    0, ... % initial time of the evolution operator
    'AYY', @(t, par) [1-par.sol(1, t).^2, -1; par.rho, -par.b*par.rho], ...
    'BYY', { @(t, par) [par.mu, 0; 0, 0] }, ...
    'par', sol_db.par, ...
    't', sol_db.mesh ...
    );

%% Compute the spectrum of the monodromy operator (Floquet multipliers)

method = eigTMNpw_method(sol_db.ncol);
mult = eigTMNpw(system, method, 'verbosity', 'info');

%% Plot results

figure
plot_multipliers(mult)
title(sprintf('\\mu_1 = %.4f, err = %.4e', mult(1), abs(mult(1)-1)))
